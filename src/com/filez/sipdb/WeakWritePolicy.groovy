package com.filez.sipdb

import me.prettyprint.cassandra.service.OperationType;
import me.prettyprint.hector.api.ConsistencyLevelPolicy;
import me.prettyprint.hector.api.HConsistencyLevel;


class WeakWritePolicy implements ConsistencyLevelPolicy {

	@Override
	public HConsistencyLevel get(OperationType op) {
		if (op == OperationType.READ)
			return HConsistencyLevel.ONE
		else
			return HConsistencyLevel.ANY
	}

	@Override
	public HConsistencyLevel get(OperationType arg0, String arg1) {
		return get(arg0);
	}
}

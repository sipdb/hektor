package com.filez.sipdb

import me.prettyprint.cassandra.model.AllOneConsistencyLevelPolicy
import me.prettyprint.cassandra.serializers.IntegerSerializer
import me.prettyprint.cassandra.serializers.StringSerializer
import me.prettyprint.cassandra.service.template.ColumnFamilyTemplate
import me.prettyprint.cassandra.service.template.ColumnFamilyUpdater
import me.prettyprint.cassandra.service.template.ThriftColumnFamilyTemplate
import me.prettyprint.hector.api.Cluster
import me.prettyprint.hector.api.Keyspace
import me.prettyprint.hector.api.beans.HColumn
import me.prettyprint.hector.api.factory.HFactory

class Workload implements Runnable {

	private int rows
	private Keyspace keyspace
	private double writeratio
	private double notfound
	private ColumnFamilyTemplate<Integer, String> template
	private int batchsize;
	private boolean verbose

	public Workload(Cluster c, String keyspace, int rows, double ratio, double notfound)
	{
		this.keyspace = HFactory.createKeyspace(keyspace, c, new AllOneConsistencyLevelPolicy())
		this.keyspace = HFactory.createKeyspace(keyspace, c, new WeakWritePolicy())
		template = new ThriftColumnFamilyTemplate<Integer, String>(
				this.keyspace, "sipdb", IntegerSerializer.get(), StringSerializer.get())
		this.rows = rows
		this.writeratio = ratio
		this.notfound = notfound
		this.batchsize = 10000
		this.verbose = false
	}

	@Override
	public void run() {
		int i = batchsize
		int rid
		// HColumn<String, String> res
		def rnd = new Random()
		def ser = StringSerializer.get()
		ColumnFamilyUpdater<Integer, String> updater;
		StringSerializer ss = StringSerializer.get()

		while(i-- > 0) {
			rid = 1+rnd.nextInt(rows)
			if (rnd.nextDouble() < writeratio) {
				updater = template.createUpdater(rid)
				updater.setString("kam","${rnd.nextInt(rows)+1}@customer${rnd.nextInt(999)}.sip.ourdomain.net")
				template.update(updater)
			} else {
				if (rnd.nextDouble() < notfound )
					rid += rows // make sure that row will not be found
				def query = HFactory.createSliceQuery(keyspace,IntegerSerializer.get(),ss,ss)
				query.setColumnFamily("sipdb")
				query.setColumnNames("kam")
				query.setKey(rid)
				def qres = query.execute()
				def res = qres.get()
				def column = res.getColumnByName("kam")
				if (verbose) {
					if (column == null)
						println "key ${rid} not found"
					else
						println column.getValue()
				}
			}
		}
	}

	public int getBatchsize() {
		return batchsize;
	}

	public void setBatchsize(int nsize) {
		if ( nsize > 0 ) batchsize = nsize
	}

	public double getWriteratio() {
		return writeratio
	}

	public double getNotfoundratio() {
		return notfound
	}
}

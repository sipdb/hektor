package com.filez.sipdb

/* init cassandra db */

/*
 @GrabConfig(systemClassLoader=true)
 @Grab(group="me.prettyprint",module="hector-core",version="1.0-1")
 */

import me.prettyprint.cassandra.service.CassandraHostConfigurator
import me.prettyprint.hector.api.Cluster
import me.prettyprint.hector.api.factory.HFactory

TARGETROWS = 2000000
ADDROWS = 0
BATCHUPDATE = true
KEYSPACE = "test"
THREADS = 12
BATCHSIZE = 20000

rows = 0

h = new CassandraHostConfigurator("localhost:9160")
h.setCassandraThriftSocketTimeout(0)
Cluster c = HFactory.getOrCreateCluster("test-cluster",h);

println "cluster definition created"

/*
 Prepare.dropSchema(c, KEYSPACE)
 println "schema droped"

 Prepare.createSchema(c, KEYSPACE)

 println "schema created"
*/



//Prepare.initDataBatch(c, KEYSPACE, 1, TARGETROWS)

start = System.currentTimeMillis()
if ( rows == 0 ) {
	print "counting rows "
	rows = Prepare.countRealRows(c, KEYSPACE)
	eta = (System.currentTimeMillis()-start)/1000.0d;
	print rows
	print " in ${eta.round(2)} seconds, "
	println "${(rows/eta).intValue()} rows/sec."
}

if (ADDROWS == 0) ADDROWS = TARGETROWS - rows - 1

if (ADDROWS > 0) {
	start = System.currentTimeMillis()
	if (BATCHUPDATE)
		Prepare.initDataBatch(c, KEYSPACE, rows + 1, rows + 1 + ADDROWS)
	else
		Prepare.initData     (c, KEYSPACE, rows + 1, rows + 1 + ADDROWS)

	eta = (System.currentTimeMillis()-start)/1000.0d;
	print "Inserted ${ADDROWS+1} rows in ${eta.round(2)} seconds, "
	println "${(ADDROWS/eta).intValue()} rows/sec."
}
else
	ADDROWS = 0

workers = []
THREADS.times {
	w = new Workload(c,KEYSPACE,rows+ADDROWS,0.6, 0.1)
	w.batchsize = BATCHSIZE
	workers.add(new Thread(w))
}
println "Running workload ${w.writeratio*100}% write, ${w.notfoundratio*100}% nonfound in ${THREADS} threads ${w.batchsize} ops each."
start = System.currentTimeMillis()
workers.each { it -> it.start() }
workers.each { it -> it.join()  }
eta = (System.currentTimeMillis()-start)/1000.0d;
println "Workload took ${eta.round(2)} seconds, thruput ${(THREADS*w.batchsize/eta).round(2)} ops/sec"

print "disconnecting... "
c.getConnectionManager().shutdown();
println "done"

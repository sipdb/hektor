package com.filez.sipdb

/* init cassandra db */

/*
 @GrabConfig(systemClassLoader=true)
 @Grab(group="me.prettyprint",module="hector-core",version="1.0-1")
 */

import me.prettyprint.cassandra.service.CassandraHostConfigurator
import me.prettyprint.hector.api.Cluster
import me.prettyprint.hector.api.factory.HFactory
import me.prettyprint.hector.api.beans.OrderedRows
import me.prettyprint.cassandra.serializers.IntegerSerializer
import me.prettyprint.cassandra.serializers.StringSerializer
import me.prettyprint.hector.api.beans.Row
import me.prettyprint.hector.api.mutation.Mutator
import me.prettyprint.hector.api.query.RangeSlicesQuery
import me.prettyprint.hector.api.query.QueryResult

KEYSPACE = "test"
BATCHSIZE = 1000

h = new CassandraHostConfigurator("localhost:9160")
h.setCassandraThriftSocketTimeout(0)
Cluster c = HFactory.getOrCreateCluster("test-cluster",h);

println "cluster definition created"

int count = 0
int skip  = 0
int totalrows = 0
long start = System.currentTimeMillis()

RangeSlicesQuery <Integer,String,String> query;
query = HFactory.createRangeSlicesQuery(HFactory.createKeyspace(KEYSPACE, c, new WeakWritePolicy()),
		IntegerSerializer.get(), StringSerializer.get(), StringSerializer.get());
query.setRowCount(BATCHSIZE)
query.setColumnFamily("sipdb")
query.setColumnNames("kam")
Mutator <Integer> m = HFactory.createMutator(HFactory.createKeyspace(KEYSPACE, c, new WeakWritePolicy()),
		IntegerSerializer.get())
QueryResult<OrderedRows<Integer, String, String>> result = query.execute();
OrderedRows<Integer, String, String> rows;

while(true) {
	rows = query.execute().get()
	if (rows.getCount() < 1 + skip )
		break
	/** query returned rows counter */
	int rcount = 0;
	/** deleted rows counter */
	int dcount = 0;
	for (Row r:rows) {
		if (++rcount <= skip )
			continue
		totalrows++
		if(r.getColumnSlice().getColumnByName("kam")) {
			m.addDeletion(r.getKey(),"sipdb")
			dcount++
		}
	}
	if (dcount > 0) {
		m.execute()
		count += dcount;
		println "${totalrows} processed, ${count} rows deleted."
	}
	
	query.setKeys(rows.peekLast().getKey(), null)
	skip = 1
}

eta = (System.currentTimeMillis()-start)/1000.0d;
println "Workload took ${eta.round(2)} seconds. thruput ${(totalrows/eta).round(2)} reads/sec, ${(count/eta).round(2)} deletes/sec"

print "disconnecting... "
c.getConnectionManager().shutdown();
println "done"

package com.filez.sipdb

import me.prettyprint.cassandra.model.BasicColumnDefinition
import me.prettyprint.cassandra.model.BasicColumnFamilyDefinition
import me.prettyprint.cassandra.serializers.IntegerSerializer
import me.prettyprint.cassandra.serializers.StringSerializer
import me.prettyprint.cassandra.service.ThriftCfDef
import me.prettyprint.cassandra.service.template.ColumnFamilyTemplate
import me.prettyprint.cassandra.service.template.ColumnFamilyUpdater
import me.prettyprint.cassandra.service.template.ThriftColumnFamilyTemplate
import me.prettyprint.hector.api.Cluster
import me.prettyprint.hector.api.beans.OrderedRows
import me.prettyprint.hector.api.beans.Row
import me.prettyprint.hector.api.ddl.ComparatorType
import me.prettyprint.hector.api.exceptions.HInvalidRequestException
import me.prettyprint.hector.api.factory.HFactory
import me.prettyprint.hector.api.mutation.MutationResult
import me.prettyprint.hector.api.mutation.Mutator
import me.prettyprint.hector.api.query.QueryResult
import me.prettyprint.hector.api.query.RangeSlicesQuery


/**
 * 
 */

public class Prepare {
	
	/*
	 * cislo - integer
	 * kam - varchar(50) not null
	 * 
	 * 222@customer123.sip.ourdomain.net
	 */
	static void createSchema(Cluster c, String keyspace) {
		StringSerializer ser = StringSerializer.get();
		
		BasicColumnFamilyDefinition sipdb = new BasicColumnFamilyDefinition();
		sipdb.setKeyspaceName(keyspace)
		sipdb.setName("sipdb")
		sipdb.setComment("phone calls routing information")
		sipdb.setKeyValidationClass("org.apache.cassandra.db.marshal.Int32Type")
		sipdb.setComparatorType(ComparatorType.ASCIITYPE)
		
		BasicColumnDefinition kam = new BasicColumnDefinition();
		kam.setName(ser.toByteBuffer("kam"))
		kam.setValidationClass(ComparatorType.ASCIITYPE.getClassName())
		kam.setIndexType(null)
		
		sipdb.addColumnDefinition(kam)
		c.addColumnFamily(new ThriftCfDef(sipdb))
	}
	
	static void dropSchema(Cluster c, String keyspace) {
		try {
			c.dropColumnFamily(keyspace, "sipdb")
		}
		catch (HInvalidRequestException e) {}
	}
	
	static void initData(Cluster c, String keyspace, int first, int last) {
		ColumnFamilyTemplate<Integer, String> template = new ThriftColumnFamilyTemplate<Integer, String> 
		 		(HFactory.createKeyspace(keyspace, c, new WeakWritePolicy()), "sipdb", IntegerSerializer.get(), StringSerializer.get() );
		ColumnFamilyUpdater<Integer, String> updater;
		def rnd = new Random()
		
		for (int i=first;i<=last;i++) {
			updater = template.createUpdater(i)
			updater.setString("kam","${i}@customer${rnd.nextInt(999)}.sip.ourdomain.net")
			template.update(updater)
		}
	}

	static void initDataBatch(Cluster c, String keyspace, int first, int last) {
		Mutator <Integer> m = HFactory.createMutator(HFactory.createKeyspace(keyspace, c, new WeakWritePolicy()), 
			IntegerSerializer.get())
		MutationResult res;
		
		def BATCHSIZE = 500
		def rnd = new Random()
		int i
		println "Batch inserting: first ${first} last ${last}."
		for (i=first;i<=last;i++) {
			m.addInsertion(i,"sipdb",HFactory.createStringColumn("kam", "${i}@customer${rnd.nextInt(999)}.sip.ourdomain.net"))
			if ( i % BATCHSIZE == 0 )
			    m.execute()
		}
		res = m.execute()
	}

	static int countRows(Cluster c, String keyspace) {
		int count = 0
		int skip  = 0
		RangeSlicesQuery <Integer,String,String> query;
		query = HFactory.createRangeSlicesQuery(HFactory.createKeyspace(keyspace, c, new WeakWritePolicy()),
			IntegerSerializer.get(), StringSerializer.get(), StringSerializer.get());
		query.setReturnKeysOnly()
		query.setRowCount(5000)
		// query.setKeys(Integer.MIN_VALUE,Integer.MAX_VALUE)
		query.setColumnFamily("sipdb")
		
		QueryResult<OrderedRows<Integer, String, String>> result = query.execute();
		OrderedRows<Integer, String, String> rows;
		
		while(true) {
			rows = query.execute().get()
			if (rows.getCount() < 1 + skip )
			    break
				
			// printRows(rows,skip)
			count += rows.getCount() - skip
			query.setKeys(rows.peekLast().getKey(), null)
			skip = 1	
		}
	
		return count
	}

	static int countRealRows(Cluster c, String keyspace) {
		int count = 0
		int skip  = 0
		RangeSlicesQuery <Integer,String,String> query;
		query = HFactory.createRangeSlicesQuery(HFactory.createKeyspace(keyspace, c, new WeakWritePolicy()),
			IntegerSerializer.get(), StringSerializer.get(), StringSerializer.get());
		query.setRowCount(5000)
                query.setColumnNames("kam")
		// query.setKeys(Integer.MIN_VALUE,Integer.MAX_VALUE)
		query.setColumnFamily("sipdb")
		
		QueryResult<OrderedRows<Integer, String, String>> result = query.execute();
		OrderedRows<Integer, String, String> rows;
		
		while(true) {
			rows = query.execute().get()
                        // check if some rows were returned
			if (rows.getCount() < 1 + skip )
			    break
                        // check if "kam" column is present
		        for (Row r: rows) {
                           if (r.getColumnSlice().getColumnByName("kam") == null)
                              return count
   	         	}

			// printRows(rows,skip)
			count += rows.getCount() - skip
			query.setKeys(rows.peekLast().getKey(), null)
			skip = 1	
		}
	
		return count
	}
	
	private static int printRows(OrderedRows rows, int skip) {
		int count = 0
		for (Row r: rows) {
			if (++count <= skip )
			    continue
			
			print "key ${r.getKey()} "
		}
		println "count ${rows.getCount()}"
		return rows.getCount()
	}
}
